extends NinePatchRect

var int_escenario = 1

func _process(delta):
	$escenario.texture = load("res://Graphics/PNG/Escenarios/suelo"+str(int_escenario)+".png")
	
	if int_escenario == 1:
		global_var.style["Escenario"] = "tierra"
	elif int_escenario == 2:
		global_var.style["Escenario"] = "grama"
	elif int_escenario == 3:
		global_var.style["Escenario"] = "hielo"
	elif int_escenario == 4:
		global_var.style["Escenario"] = "piedra"
	elif int_escenario == 5:
		global_var.style["Escenario"] = "nieve"
	
	
func _on_iz_pressed():
	int_escenario -= 1
	if int_escenario <= 0:
		int_escenario = 4

func _on_der_pressed():
	int_escenario += 1
	if int_escenario > 4:
		int_escenario = 1