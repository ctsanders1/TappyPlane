extends NinePatchRect

var int_obstaculo = 1

func _process(delta):
	$obstaculos.texture = load("res://Graphics/PNG/Obstaculos/roca"+str(int_obstaculo)+".png")
	if int_obstaculo == 1:
		global_var.style["Obstaculo"] = "tierra"
	elif int_obstaculo == 2:
		global_var.style["Obstaculo"] = "grama"
	elif int_obstaculo == 3:
		global_var.style["Obstaculo"] = "nieve"
	elif int_obstaculo == 4:
		global_var.style["Obstaculo"] = "hielo"

func _on_iz_pressed():
	int_obstaculo -= 1
	if int_obstaculo <= 0:
		int_obstaculo = 4

func _on_der_pressed():
	int_obstaculo += 1
	if int_obstaculo > 4:
		int_obstaculo = 1