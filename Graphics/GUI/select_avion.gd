extends Control

var int_plane = 1
var int_obstaculo = 1

func _process(delta):
	$aviones.texture = load("res://Graphics/PNG/Planes/Select/plane"+str(int_plane)+".png")
	if int_plane == 1:
		$nombre.text = "Avión Azul"
		global_var.style["PlaneColor"] = "azul"
	if int_plane == 2:
		$nombre.text = "Avión Verde"
		global_var.style["PlaneColor"] = "verde"
	if int_plane == 3:
		$nombre.text = "Avión Rojo"
		global_var.style["PlaneColor"] = "rojo"
	if int_plane == 4:
		$nombre.text = "Avión Amarillo"
		global_var.style["PlaneColor"] = "amarillo"

func _on_iz_pressed():
	int_plane -= 1
	if int_plane <= 0:
		int_plane == 4
func _on_der_pressed():
	int_plane += 1
	if int_plane > 4:
		int_plane = 1