extends CanvasLayer

var score

func _ready():
	set_process(true)
	score = Persistence.get_data()
	if score.size() == 0:
		score["ScoreInit"] = 0
		Persistence.save_data()
	
func _process(delta):
	$score.text = "PUNTUACIṔÓN: "+str(global_var.score)
	
	if global_var.in_game == false and global_var.score > score["ScoreInit"]:
		score["ScoreInit"] = global_var.score
		Persistence.save_data()

func _on_pausa_pressed():
	if get_tree().paused:
		get_tree().paused = false
	elif get_tree().paused == false:
		get_tree().paused = true

func _on_menu_pressed():
	get_tree().change_scene("res://Levels/escena_select.tscn")