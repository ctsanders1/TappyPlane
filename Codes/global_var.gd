extends Node

var style = {"ColorPlane": null, "Escenario": null, "Obstaculo": null}
var score = 0
var in_game = true

func _ready():
	self.pause_mode = PAUSE_MODE_PROCESS
	set_process(true)

func _process(delta):
	if in_game == false:
		get_tree().paused = true