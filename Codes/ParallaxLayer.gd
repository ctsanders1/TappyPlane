extends ParallaxLayer

var speed_offset = -400

func _ready():
	set_process(true)

func _process(delta):
	get_parent().scroll_offset.x += speed_offset*delta
