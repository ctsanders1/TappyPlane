extends RigidBody2D

func _ready():
	# Color por defecto
	$spr_anim.animation = global_var.style["PlaneColor"]
	
	set_process(true)
	set_process_input(true)

func _input(event):
	if event is InputEventScreenTouch or event is InputEventMouseButton:
		_move()
		
func _process(delta):
	
	if global_var.in_game == false and $"../game_over".visible == false:
		$"../game_over".visible = true
		$"../CanvasLayer".queue_free()
		
		var score = Persistence.get_data()
		
		$"../game_over/pa".text = "Puntuación actual: "+str(global_var.score)
		$"../game_over/pm".text = "Puntuación máxima: "+str(score["ScoreInit"])
		
		if $"../game_over/anim".is_playing() == false:
			$"../game_over/anim".play("game_over")
			yield($"../game_over/anim","animation_finished")
			$"../game_over/anim".stop()

func _move():
	linear_velocity = Vector2(0,-240)

# Volver a abrir la misma escena
func _on_repeat_pressed():
	global_var.in_game = true
	get_tree().paused = false
	global_var.score = 0
	get_tree().reload_current_scene()
