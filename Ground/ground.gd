extends Node2D

export var dir = 1

func _ready():
	set_process(true)
	if global_var.style["Escenario"] != null:
		$ground/spr.animation = global_var.style["Escenario"]

func _process(delta):
	move_local_x(dir*-410*delta)
	
	if global_position.x <= -404:
		global_position.x = -404 + 800*2