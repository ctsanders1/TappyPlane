extends Area2D

func _ready():
	$spr.frame = int(rand_range(-1,3))
	set_process(true)

func _process(delta):
	move_local_x(-300*delta)

func _on_star_body_entered(body):
	global_var.score += $spr.frame + 1
	queue_free()
