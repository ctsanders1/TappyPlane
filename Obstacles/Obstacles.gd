extends Node2D

func _ready():
	set_process(true)
	if global_var.style["Obstaculo"] != null:
		$obstacle_up/spr.animation = global_var.style["Obstaculo"]
		$obstacle_down/spr.animation = global_var.style["Obstaculo"]

func _process(delta):
	move_local_x(-300*delta)
	
	if global_position.x <= -404:
		queue_free()

func _on_area_col_body_entered(body):
	if body is RigidBody2D:
		global_var.in_game = false
