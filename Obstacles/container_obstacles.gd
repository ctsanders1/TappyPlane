extends Node2D

var obstacle = preload("res://Obstacles/Obstacles.tscn")
var star = preload("res://Star/star.tscn")

func _ready():
	set_process(true)

func _process(delta):
	if $time_spawn.is_stopped():
		_spawn()
		$time_spawn.start()

func _spawn():
	var new_obstacle = obstacle.instance()
	add_child(new_obstacle)
	new_obstacle.global_position.x = 900
	new_obstacle.global_position.y = rand_range(-68,80)
	
	var new_star = star.instance()
	add_child(new_star)
	new_star.global_position = Vector2(new_obstacle.global_position.x, new_obstacle.global_position.y+250)